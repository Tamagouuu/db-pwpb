-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 25, 2022 at 02:55 AM
-- Server version: 5.7.33
-- PHP Version: 8.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpmvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `penulis` int(11) DEFAULT NULL,
  `judul` varchar(255) NOT NULL,
  `tulisan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `penulis`, `judul`, `tulisan`) VALUES
(1, 1, 'Belajar PHP MVC', 'Tutorial PHP MVC'),
(2, 2, 'Belajar OOP PHP\r\n', 'Tutorial OOP PHP\r\n'),
(3, 1, 'Belajar PHP Dasar\r\n', 'Tutorial PHP Dasar\r\n'),
(5, 13, 'Belajar Java', 'Belajar java persama gautama ganteng'),
(6, 13, 'Halo', 'asdasds'),
(7, 14, 'Belajar membuat sesuatu', 'sesuatu membuat Belajar');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nrp` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `jurusan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nama`, `nrp`, `email`, `jurusan`) VALUES
(1, 'I Made Gautama', '0490378375', 'imadegautama@gmail.com', 'Teknik Planologi'),
(2, 'I Wayan Ramagastia', '0490378376', 'ramagastia@gmail.com', 'Teknik Informatika'),
(3, 'Ketut Jamprut', '0490378377', 'tutjamprut@gmail.com', 'Teknik Persawahan'),
(10, 'I Wayan Ramagastia', '0490378376', 'ramagastia@gmail.com', 'Teknik Planologi'),
(11, 'I Made Gautama', '0490378375', 'imadegautama@gmail.com', 'Teknik Pangan'),
(12, 'Sumantoro', '99', 'minecraftgautama@gmail.com', 'Teknik Informatika');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama_penulis` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama_penulis`, `email`, `password`) VALUES
(1, 'Gautama', 'gautama@gmail.com', 'tama'),
(2, 'Linux', 'linux@gmail.com', 'linux'),
(11, 'gautama', 'gautama18@gmail.com', 'd37921327ae5a8e6fc55ce469e77e521'),
(12, 'arya', 'aryaadi@gmail.com', '4c3303f87bae4775b9212a155ce0ea6e'),
(13, 'Gautam', 'minecraftgautama12@gmail.com', '4c3303f87bae4775b9212a155ce0ea6e'),
(14, 'admin', 'admin@localhost.com', '8db044147bbec7949c523e9dd1adb03f'),
(15, 'Gautam', 'gautamalala@gmail.com', '514cb09ea5a4de393bf0842acf4bee2b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penulis` (`penulis`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `blog_ibfk_1` FOREIGN KEY (`penulis`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
